const nextJest = require('next/jest');

const createJestConfig = nextJest({
	dir: './',
});

/** @type {import('jest').Config} */
const customJestConfig = {
	setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
	moduleDirectories: ['node_modules', '<rootDir>/'],
	testEnvironment: 'jest-environment-jsdom',
	fakeTimers: {
		enableGlobally: true,
	},
	coveragePathIgnorePatterns: ['/node_modules/'],
	coverageThreshold: {
		global: {
			branches: 100,
			functions: 100,
			lines: 100,
		},
	},
};

module.exports = createJestConfig(customJestConfig);
