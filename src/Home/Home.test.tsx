import { render, screen } from '@testing-library/react';
import { Home } from './Home';

describe('Home', () => {
	test('Home should exists', () => {
		render(<Home />);

		expect(screen.getByText(/^Tips de programación$/));
	});
});
