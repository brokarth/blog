import { FC } from 'react';

export const UserInteractions: FC = () => (
	<section>
		<h2>Implementación de interacciones de usuario</h2>
		<ul>
			<li>Mobile First</li>
			<li>Keyboard first</li>
		</ul>
	</section>
);
