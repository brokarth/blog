import { FC } from 'react';

export const HexagonalArchitecture: FC = () => (
	<section>
		<h2>Implementación de arquitectura hexagonal</h2>
		<ul>
			<li>
				En <em>src/</em>, agregar una carpeta por cada pantalla
			</li>
			<li>Test unitario a nivel de pantalla con perspectiva de usuario</li>
			<li>
				Archivo de estilo directamente en la carpeta de la pantalla o componente
			</li>
			<li>
				Sub componentes dentro de <em>src/screen/components</em>
			</li>
			<li>
				Infraestructura (Clientes con conexión a clientes) dentro de{' '}
				<em>src/screen/clients</em>
			</li>
			<li>
				Modelos dentro de <em>src/screen/models</em>
			</li>
		</ul>
	</section>
);
