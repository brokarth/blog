import { FC } from 'react';

export const GoodPractices: FC = () => (
	<section>
		<h2>Buenas practicas</h2>
		<ul>
			<li>Ejecutar test con coverage en cada commit</li>
			<li>Formatear test en cada commit</li>
			<li>Ejecutar reglas estáticas en cada commit</li>
			<li>No tener mas de tres niveles de anidación</li>
		</ul>
	</section>
);
