import { FC } from 'react';

export const FunctionalTesting: FC = () => (
	<section>
		<h2>Testing funcional</h2>
		<ul>
			<li>Test de regresión visual</li>
			<li>Test de e2e en ambiente de stage</li>
			<li>Test de calidad web (lighthouse)</li>
		</ul>
	</section>
);
