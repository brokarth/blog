import { FC } from 'react';

export const UnitTesting: FC = () => (
	<section>
		<h2>Testing unitario</h2>
		<ul>
			<li>TDD como técnica base para crear código.</li>
			<li>1 Test unitario a nivel de pantalla, no de componentes.</li>
			<li>100% coverage (no tiene por haber código sin validar que hace)</li>
			<li>
				Se debe probar desde la perspectiva del usuario que representa la unidad
				lógica minima que aporta valor a negocio. Testing de subcomponentes o
				infraestructura es mala practica ya que se escapan detalles de
				implementación
			</li>
		</ul>
	</section>
);
