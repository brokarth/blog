import type { NextPage } from 'next';
import Head from 'next/head';
import { FunctionalTesting } from './components/FunctionalTesting';
import { GoodPractices } from './components/GoodPractices';
import { HexagonalArchitecture } from './components/HexagonalArchitecture';
import { UnitTesting } from './components/UnitTesting';
import { UserInteractions } from './components/UserInteractions';
import styles from './Home.module.scss';

export const Home: NextPage = () => (
	<main id={styles['home']}>
		<Head>
			<title>Blog de brokarth</title>
			<meta name="description" content="Blog de brokarth" />
			<link rel="icon" href="/favicon.ico" />
		</Head>

		<h1>Tips de programación</h1>

		<HexagonalArchitecture />
		<UnitTesting />
		<FunctionalTesting />
		<UserInteractions />
		<GoodPractices />
	</main>
);
